<?php

class bddconnect
{
    var $mysqli;

    public function __construct()
    {
        $this->mysqli = new mysqli("localhost", "root", "", "oscup");
    }

    public function getConnection()
    {
        return $this->mysqli;
    }

    public function close()
    {
        $this->mysqli->close();
    }
}

?>