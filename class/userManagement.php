<?php
class UserManagement 
{


    /********************************************
    *        UserManagement                  	*
    *********************************************
    * Status :                               	*
    * 		18	: Admin						    *
    * 		1	: Member						*
    ********************************************/
    public function __construct()
    {

    }

    //Function to send Cookies
    function EnvoieCookie($UserID,$Password,$Permission,$id)
    {
        SetCookie("ZoneAdminOSCup","$UserID:$Password:$Permission:$id",time()+3600*24*365); 
        SetCookie("ZoneAdminOSCupLogin","$UserID",time()+3600*24*365);
    }

    // Function to check the Session
    function VerifSession($UserID,$Password)
    {
        if(empty($UserID) || empty($Password)) return 0;
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT status FROM player WHERE name=? and password=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("ss", $UserID, $Password);

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt_result = $stmt->get_result();
        if($stmt_result->num_rows > 0) {
            $data = $stmt_result->fetch_assoc();
            return $data["status"];
        }

        return 0;
    }

    function VerifAdminSession($userID, $password)
    {
        if(empty($userID) || empty($password)) return 0;
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT status FROM player WHERE name=? and password=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("ss", $userID, $password);

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt_result = $stmt->get_result();
        if($stmt_result->num_rows > 0) {
            $data = $stmt_result->fetch_assoc();
            if($data["status"]=18) return 1;
        }

        return 0;
    }

    // Generate a random Password
    function GenerPassword()
    {
        $string="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $pass="";
        for($i=0;$i<8;$i++) {
            $pass.=$string[mt_rand()%strlen($string)]; 
        }
        return $pass;
    }

    // Login Mod
    function LoginMod($table_membres) {
        $auth=explode(':',$_COOKIE['ZoneAdminOSCup']); 
        if(!empty($auth[0]) || !empty($auth[1]) || !empty($auth[2]) || !empty($auth[3]) || !empty($auth[4])) {
            $Permission=$this->VerifSession($auth[0],$auth[1], $table_membres);
    
            if(!$Permission || $Permission != $auth[2]) {
                setcookie('ZoneAdminOSCup');
                header('Location: index.php');
                exit(); 
            }
        }
        elseif(isset($_POST['posted'])) {

            $this->connectUser();
        }
        
    }

    function connectUser()
    {
        $Password=md5($_POST['password']);
        $userID = $_POST['login'];
        if(!empty($userID) and !empty($Password))
        {
            $Permission=$this->VerifSession($userID,$Password);
            $bddconnect = new bddconnect;
            $mysqli = $bddconnect->getConnection();

            //Perform a query to get the last tournament
            if (!($stmt = $mysqli->prepare("SELECT id FROM player WHERE name=? and password=?"))) {
                echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            }

            $stmt->bind_param("ss", $userID, $Password);
            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
            $id=0;

            $stmt_result = $stmt->get_result();
            if($stmt_result->num_rows > 0) {
                $data = $stmt_result->fetch_assoc();
                $id=$data["id"];
                echo "id = ".$data["id"];
                $this->EnvoieCookie($userID,$Password,$Permission,$id);
                header('Location: index.php');
                exit(); 
            }
        }   
    }
}

?>