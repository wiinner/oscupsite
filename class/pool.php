<?php
include_once("matchs.php");

class Pool 
{
    public $id;
    public $tournament_id;
    public $name;
    public $listTeams;
    public $listMatchs;
    public $type;


    public function __construct()
    {

    }

    //untested yet
    public function getPool($id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament

        if (!($stmt = $mysqli->prepare("SELECT id, name FROM pool WHERE id=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            $row = $res->fetch_assoc();
            $this->id = $row['id'];
            $this->name = $row['name'];
        } else {
            echo "no result";
        }
        return $this;
    }

    public function getAllPoolFromTournament($tournament_id, $poolType = 1)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT id, tournament_id, name FROM pool WHERE tournament_id=? and type=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("ii", $tournament_id, $poolType);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $listPools = array();
        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {
                //For everyPool, we need to get the teams
                
                $this->id = $row['id'];
                $this->tournament_id = $tournament_id;
                $this->name = $row['name'];
                $this->type = $poolType;
                $team = new Team;
                $this->listTeams = $team->getAllTeamFromPool($this->id);
                $match = new Match;
                $this->listMatchs = $match->getAllMatchFromPool($this->id);

                array_push($listPools, $this->toArray());
            }
        }
        return $listPools;

    }

    public function save(&$err)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        $mysqli->begin_transaction();
        try {
            $stmt = $mysqli->prepare("INSERT INTO pool(name, tournament_id, type) VALUES(?,?,?)");
            $stmt->bind_param('sii', $this->name, $this->tournament_id, $this->type);
    
            $res = $stmt->execute();
            if(!$res) {
                $err .= "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                return 0;
            } else {
                $mysqli->commit();
                $err = "";
                return $stmt->insert_id; //ID avec the record
            }
        } catch(mysqli_sql_exception $exception)
        {
            $mysqli->rollback();
            $err .= "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return -1;
        }
        return -1;

    }

    public function toArray()
    {
        $array['id'] = $this->id;
        $array['name'] = $this->name;
        $array["type"] = $this->type;
        $array['teams'] = $this->listTeams;
        $array['matchs'] = $this->listMatchs;

        return $array; 
    }
    //region Getters and Setters below
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }
    


    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }
    //endregion
}

?>