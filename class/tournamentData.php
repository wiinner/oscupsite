<?php

class TournamentData
{
    protected $id;
    protected $name;
    protected $date;
    protected $subscription_date_start;
    protected $subscription_date_end;
    protected $day;
    protected $month;
    protected $year;
    protected $tournamentTypeName;

    public function __construct()
    {

    }

    //region Getters and Setters below
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }
    


    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of subscription_date_start
     */ 
    public function getSubscription_date_start()
    {
        return $this->subscription_date_start;
    }

    /**
     * Set the value of subscription_date_start
     *
     * @return  self
     */ 
    public function setSubscription_date_start($subscription_date_start)
    {
        $this->subscription_date_start = $subscription_date_start;

        return $this;
    }

    /**
     * Get the value of subscription_date_end
     */ 
    public function getSubscription_date_end()
    {
        return $this->subscription_date_end;
    }

    /**
     * Set the value of subscription_date_end
     *
     * @return  self
     */ 
    public function setSubscription_date_end($subscription_date_end)
    {
        $this->subscription_date_end = $subscription_date_end;

        return $this;
    }

    /**
     * Get the value of day
     */ 
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set the value of day
     *
     * @return  self
     */ 
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get the value of month
     */ 
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set the value of month
     *
     * @return  self
     */ 
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get the value of year
     */ 
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */ 
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of tournamentTypeName
     */ 
    public function getTournamentTypeName()
    {
        return $this->tournamentTypeName;
    }

    /**
     * Set the value of tournamentTypeName
     *
     * @return  self
     */ 
    public function setTournamentTypeName($tournamentTypeName)
    {
        $this->tournamentTypeName = $tournamentTypeName;

        return $this;
    }
    //endregion
}

?>
