<?php

class Player 
{
    public $id;
    public $name;

    //untested yet
    public function __construct()
    {

    }

    //untested yet
    public function getPlayer($player_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament

        if (!($stmt = $mysqli->prepare("SELECT id, player.name FROM player WHERE id=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $player_id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            $row = $res->fetch_assoc();
            $this->id = $row['id'];
            $this->name = $row['name'];
        } else {
            echo "no result";
        }
        return $this;
    }

    public function toArray()
    {
        $array['id'] = $this->id;
        $array['name'] = $this->name;
        return $array; 
    }

    public function requestToAccess($requestPage)
    {
        $auth=explode(':',$_COOKIE['ZoneAdminOSCup']);
        if(!empty($auth[0]) || !empty($auth[1]) || !empty($auth[2]) || !empty($auth[3])) { return 0; } //No user
        if($auth[2] != 18) { return 0; } //The user is not connected as an admin
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament

        if (!($stmt = $mysqli->prepare("SELECT id, player.name FROM player WHERE id=? AND name=? and password=? and status=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
        $stmt->bind_param('issi', $auth[3], $auth[0], $auth[1], $auth[2]);
        $stmt->execute();
        $res = $stmt->get_result();
        if($res->num_rows != 1) { return 0; } //if there is 0 record found, that is an attempt to penetrate the system

        switch($requestPage)
        {
            default: //For the moment, admin can access everyhing. Later if we want to  have a more detailed admin rights, it would be possible
                return 1;
            break;
        }
    }

    //region Getters and Setters below
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }
    


    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    //endregion
}

?>
