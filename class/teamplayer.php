<?php
include_once("player.php");

class TeamPlayer 
{
    public $player_id;
    public $team_id;
    public $player_name;

    public $listIdPlayer;

    public function __construct()
    {
        $this->listIdPlayer = array();
    }

    //untested yet
    public function getTeamPlayerList($team_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT player_id FROM teamplayer WHERE team_id=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $team_id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            $this->listIdTeam = array();
            while ($row = $res->fetch_assoc()) {
                array_push($this->listIdPlayer, $row['player_id']);
            }
        } else {
            $this->listIdPlayer = array();
        }
        return $this;
    }


    public function getAllInfoTeamPlayer($player_id)
    {
        $player = new Player;
        $player->getPlayer($player_id);
        return $player->toArray();
    }

    public function getAllPlayerFromTeam($team_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT player_id, team_id FROM teamplayer WHERE team_id=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $team_id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $listPlayers = array();
        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {
                //For everyPool, we need to get the teams
                
                $this->player_id = $row['player_id'];
                $this->team_id = $team_id;
                $player = new Player;
                $this->player_name = $player->getPlayer($this->player_id)->name;

                array_push($listPlayers, $this->toArray());
            }
        }
        return $listPlayers;
    }

    public function save(&$err)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        $mysqli->begin_transaction();
        try {
            $stmt = $mysqli->prepare("INSERT INTO teamplayer(player_id, team_id) VALUES(?,?)");
            $stmt->bind_param('ii', $this->player_id, $this->team_id);
    
            $res = $stmt->execute();
            if(!$res) {
                $err .= "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                return 0;
            } else {
                $mysqli->commit();
                $err = "";
                return 1; //1 pour dire OK
            }
        } catch(mysqli_sql_exception $exception)
        {
            $mysqli->rollback();
            $err .= "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return -1;
        }
        return -1;

    }

    public function toArray()
    {
        $array['player_id'] = $this->player_id;
        $array['player_name'] = $this->player_name;
        $array['team_id'] = $this->team_id;
        return $array;
    }

    //region Getters and Setters below
    /**
     * Get the value of player_id
     */ 
    public function getPlayer_id()
    {
        return $this->player_id;
    }

    /**
     * Set the value of player_id
     *
     * @return  self
     */ 
    public function setPlayer_id($player_id)
    {
        $this->player_id = $player_id;

        return $this;
    }

    /**
     * Get the value of team_id
     */ 
    public function getTeam_id()
    {
        return $this->team_id;
    }

    /**
     * Set the value of team_id
     *
     * @return  self
     */ 
    public function setTeam_id($team_id)
    {
        $this->team_id = $team_id;

        return $this;
    }

    /**
     * Get the value of listIdPlayer
     */ 
    public function getListIdPlayer()
    {
        return $this->listIdPlayer;
    }

    /**
     * Set the value of listIdPlayer
     *
     * @return  self
     */ 
    public function setListIdPlayer($listIdPlayer)
    {
        $this->listIdPlayer = $listIdPlayer;

        return $this;
    }
    //endregion

    

    
}

?>