<?php
include("tournamentPool.php");
include_once("tournamentData.php");
class Tournament 
{
    public $id;
    public $name;
    public $date;
    public $subscription_date_start;
    public $subscription_date_end;
    public $day;
    public $month;
    public $year;

    public $listPool;

    public function __construct()
    {

    }

    //tested
    public function getLastTournament()
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT id, tournament.name, tournament.date, subscription_date_start, subscription_date_end, tournament.day, tournament.month, tournament.year FROM tournament ORDER BY 'date' DESC LIMIT 0,1"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
       
        if($res->num_rows > 0) {
            $row = $res->fetch_assoc();
            $this->id = $row['id'];
            $this->name = $row['name'];
            $this->date = $row['date'];
            $this->subscription_date_start = $row['subscription_date_start'];
            $this->subscription_date_end = $row['subscription_date_end'];
            $this->day = $row['day'];
            $this->month = $row['month'];
            $this->year = $row['year'];
        } else {
            echo "no result";
        }
        return $this;
    }

    public function getTournament($tournament_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT id, tournament.name, tournament.date, subscription_date_start, subscription_date_end, tournament.day, tournament.month, tournament.year FROM tournament WHERE id=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param('i', $tournament_id);

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
       
        if($res->num_rows > 0) {
            $row = $res->fetch_assoc();
            $this->id = $row['id'];
            $this->name = $row['name'];
            $this->date = $row['date'];
            $this->subscription_date_start = $row['subscription_date_start'];
            $this->subscription_date_end = $row['subscription_date_end'];
            $this->day = $row['day'];
            $this->month = $row['month'];
            $this->year = $row['year'];
        } else {
            echo "no result";
        }
        return $this;

    }

    public function listAllTournament()
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT tournament.id, tournament.name, tournament.date, subscription_date_start, subscription_date_end, tournament.day, tournament.month, tournament.year, tournamenttype.name as tournamentTypeName FROM tournament INNER JOIN tournamenttype ON tournament.tournament_type_id = tournamenttype.id ORDER BY 'date' DESC"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        $arrayTournament = array();
        if($res->num_rows > 0) {
            
            while($row = $res->fetch_assoc())
            {
                $tournamentData = new TournamentData;
                $tournamentData->setId($row['id']);
                $tournamentData->setName($row['name']);
                $tournamentData->setDate($row['date']);
                $tournamentData->setSubscription_date_start($row['subscription_date_start']);
                $tournamentData->setSubscription_date_end($row['subscription_date_end']);
                $tournamentData->setDay($row['day']);
                $tournamentData->setMonth($row['month']);
                $tournamentData->setYear($row['year']);
                $tournamentData->setTournamentTypeName($row['tournamentTypeName']);
                array_push($arrayTournament, $tournamentData);
            }
            
        }
        return $arrayTournament;
    }

    public function getAllInfoTournament()
    {
        $pool = new Pool;
        return $pool->getAllPoolFromTournament($this->id);
    }

    public function save(&$err)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        $stmt = $mysqli->prepare("INSERT INTO tournament(name, date, subscription_date_start, subscription_date_end, day, month, year) VALUES(?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('ssssiii', $this->name, $this->date, $this->subscription_date_start, $this->subscription_date_end, $this->day, $this->month, $this->year);

        $res = $stmt->execute();
        if(!$res) {
            $err .= "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return 0;
        } else {
            $err = ''; //Erasing the value
            return 1;
        }

    }

    //region Getters and Setters below
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }
    


    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of subscription_date_start
     */ 
    public function getSubscription_date_start()
    {
        return $this->subscription_date_start;
    }

    /**
     * Set the value of subscription_date_start
     *
     * @return  self
     */ 
    public function setSubscription_date_start($subscription_date_start)
    {
        $this->subscription_date_start = $subscription_date_start;

        return $this;
    }

    /**
     * Get the value of subscription_date_end
     */ 
    public function getSubscription_date_end()
    {
        return $this->subscription_date_end;
    }

    /**
     * Set the value of subscription_date_end
     *
     * @return  self
     */ 
    public function setSubscription_date_end($subscription_date_end)
    {
        $this->subscription_date_end = $subscription_date_end;

        return $this;
    }

    /**
     * Get the value of day
     */ 
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set the value of day
     *
     * @return  self
     */ 
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get the value of month
     */ 
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set the value of month
     *
     * @return  self
     */ 
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get the value of year
     */ 
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */ 
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }
    //endregion
}

?>
