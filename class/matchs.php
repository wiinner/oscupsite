<?php

class Match 
{
    public $matchHeader;
    public $matchResult;


    public function __construct()
    {
        $this->matchResult = array();
    }

    //untested yet
    public function getMatch($match_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament

        if (!($stmt = $mysqli->prepare("SELECT team1_id, team2_id, match_type_id, match_order FROM match WHERE id=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $match_id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            $row = $res->fetch_assoc();
            $this->id = $row['id'];
            $this->name = $row['name'];
        } else {
            echo "no result";
        }
        return $this;
    }


    public function getAllMatchFromPool($pool_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT match_id, bo_number, team1_id, team2_id, score_team1_id, score_team2_id, depth FROM matchresult WHERE pool_id=? GROUP BY match_id ORDER BY match_id ASC, depth ASC"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $pool_id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $listMatchs = array();
        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {
                $this->matchHeader = new MatchHeader;
                $this->matchHeader->pool_id = $pool_id;
                $this->matchHeader->match_id = $row["match_id"];
                $this->matchHeader->team1_id = $row["team1_id"];
                $this->matchHeader->team2_id = $row["team2_id"];
                $team = new Team;
                $this->matchHeader->team1_name = $team->getTeam($this->matchHeader->team1_id)->getName();
                $this->matchHeader->team2_name = $team->getTeam($this->matchHeader->team2_id)->getName();
                $this->matchHeader->depth = $row["depth"];

                $this->getResultFromMatch($this->matchHeader->match_id);

                array_push($listMatchs, $this->toArray());
            }
        }
        return $listMatchs;
    }

    public function getResultFromMatch($match_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT match_id, bo_number, team1_id, team2_id, score_team1_id, score_team2_id, depth FROM matchresult WHERE match_id=? ORDER BY bo_number ASC"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $match_id);

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            $this->matchResult = array();
            while ($row = $res->fetch_assoc()) {
                $matchResult = new MatchResult;
                $matchResult->pool_id = $this->matchHeader->pool_id;
                $matchResult->match_id = $match_id;
                $matchResult->bo_number = $row["bo_number"];
                $matchResult->team1_id = $this->matchHeader->team1_id;
                $matchResult->team2_id = $this->matchHeader->team2_id;
                $matchResult->score_team1 = $row["score_team1_id"];
                $matchResult->score_team2 = $row["score_team2_id"];


                array_push($this->matchResult, $matchResult->toArray());
            }
        }
    }

    public function toArray()
    {
        $array['match_id'] = $this->matchHeader->match_id;
        $array['pool_id'] = $this->matchHeader->pool_id;
        $array['team1_id'] = $this->matchHeader->team1_id;
        $array['team2_id'] = $this->matchHeader->team2_id;
        $array['team1_name'] = $this->matchHeader->team1_name;
        $array['team2_name'] = $this->matchHeader->team2_name;
        $array['depth'] = $this->matchHeader->depth;
        
        $array['result'] = $this->matchResult;
        return $array; 
    }
    //region Getters and Setters below
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }
    


    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }
    //endregion
}

class MatchHeader
{
    public $match_id;
    public $pool_id;
    public $team1_id;
    public $team2_id;
    public $team1_name;
    public $team2_name;
    public $depth;


    public function __construct()
    {

    }
}

class MatchResult
{
    public $match_id;
    public $pool_id;
    public $bo_number;
    public $team1_id;
    public $team2_id;
    public $score_team1;
    public $score_team2;


    public function __construct()
    {

    }

    public function toArray()
    {
        $array["match_id"] = $this->match_id;
        $array["pool_id"] = $this->pool_id;
        $array["bo_number"] = $this->bo_number;
        $array["team1_id"] = $this->team1_id;
        $array["team2_id"] = $this->team2_id;
        $array["score_team1"] = $this->score_team1;
        $array["score_team2"] = $this->score_team2;
        return $array;

    }
}

?>