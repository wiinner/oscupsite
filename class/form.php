<?php

class Form 
{
    //untested yet
    public function __construct()
    {

    }

    public function getPostOrGet($name)
    {

        if(isset($_POST[$name])) 
        { 
            return $_POST[$name]; 
        }

        if(isset($_GET[$name])) 
        { 
            return $_GET[$name]; 
        }
        return -1;
    }

    public function selectFromTable($table, $nomForm, $id, $name)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT ".$id.", ".$name." FROM ".$table." ORDER BY ".$name." ASC"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        $listResult = array();
        if($res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {
                $listResult[$row[$id]] = $row[$name];
            }
        }
        $return = '<select name="'.$nomForm.'">';
        foreach($listResult as $id => $name) {
            $return .= '<option value="'.$id.'">'.$name.'</option>';
        }
        $return .= '</select>';
        return $return;
    }

}
?>