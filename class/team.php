<?php
include ("teamplayer.php");

class Team 
{
    public $id;
    public $name;
    public $pool_id;
    public $listPlayers;

    //untested yet
    public function __construct()
    {
        $this->listPlayers = array();
    }

    //untested yet
    public function getTeam($team_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament

        if (!($stmt = $mysqli->prepare("SELECT id, team.name, pool_id FROM team WHERE id=?"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $team_id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            $row = $res->fetch_assoc();
            $this->id = $row['id'];
            $this->name = $row['name'];
            $this->pool_id = $row['pool_id'];
        } else {
            echo "no result";
        }
        return $this;
        //return $this;
    }

    public function getAllInfoTeam($team_id)
    {
        $listPlayerArray = array();
        $TeamArray = $this->getTeam($team_id)->toArray();
        $teamplayer = new TeamPlayer;
        $listIdPlayer = $teamplayer->getTeamPlayerList($this->id)->getListIdPlayer();
        $nbPlayer = count($listIdPlayer);
        for($i=0; $i<$nbPlayer; $i++) {
            array_push($listPlayerArray, $teamplayer->getAllInfoTeamPlayer($listIdPlayer[$i]));
        }
        $TeamArray["Players"] = $listPlayerArray;
        return $TeamArray;
        //$array["Players"] = $teamplayer->getAllInfoTeamPlayer($this->getList)
    }

    public function getAllTeamFromPool($pool_id)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        if (!($stmt = $mysqli->prepare("SELECT id, name FROM team WHERE pool_id=? ORDER BY id ASC"))) {
            echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }

        $stmt->bind_param("i", $pool_id);


        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $listTeams = array();
        $res = $stmt->get_result();
        if($res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {

                $listIdPlayer = array();
                //For everyPool, we need to get the teams
                $this->id = $row['id'];
                $this->pool_id = $pool_id;
                $this->name = $row['name'];
                $this->listPlayers = array();

                $teamplayer = new TeamPlayer;
                $listIdPlayer = $teamplayer->getTeamPlayerList($this->id)->getListIdPlayer();
                $nbPlayer = count($listIdPlayer);
                for($i=0; $i<$nbPlayer; $i++) {
                    array_push($this->listPlayers, $teamplayer->getAllInfoTeamPlayer($listIdPlayer[$i]));
                }
                array_push($listTeams, $this->toArray());
            }
        }
        return $listTeams;
    }

    public function save(&$err)
    {
        $bddconnect = new bddconnect;
        $mysqli = $bddconnect->getConnection();

        //Perform a query to get the last tournament
        $mysqli->begin_transaction();
        try {
            $stmt = $mysqli->prepare("INSERT INTO team(name, pool_id) VALUES(?,?)");
            $stmt->bind_param('si', $this->name, $this->pool_id);
    
            $res = $stmt->execute();
            if(!$res) {
                $err .= "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                return 0;
            } else {
                $err = '';
                $mysqli->commit();
                return $stmt->insert_id; //ID avec the record
            }
        } catch(mysqli_sql_exception $exception)
        {
            $mysqli->rollback();
            $err .= "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        return -1;

    }

    public function toArray()
    {
        $array['id'] = $this->id;
        $array['name'] = $this->name;
        $array['pool_id'] = $this->pool_id;
        $array["Players"] = $this->listPlayers;
        return $array; 
    }



    //region Getters and Setters below
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }
    


    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    //endregion
}

?>
