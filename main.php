<h1>Bienvenue !</h1>
    <h2>Qu'est-ce que ce site ?</h2>
    <p id="p1">Ce site a été créé en complément du Discord pour un tournoi mensuel qui se nomme OS Cup. Ce tournoi est un tournoi chill, avec des participants de tous niveaux, de Platine à
        Grand Champion. Les inscriptions sont individuelles et les équipes sont tirées au sort, de sorte à ce qu'elles soient plus ou moins toutes équilibrées. Les équipes sont annoncées
        une semaine avant le tournoi, qui a lieu le dernier samedi du mois. Cela leur laisse ainsi le temps d'apprendre à se connaître, de jouer un peu ensemble et d'organiser des scrims
        avec les autres équipes. Le tournoi se compose de 2 phases : Les phases de poules et les phases finales. Il y a deux poules de 4 équipes. Chaque équipe affronte les autres équipes
        de sa poule. A la fin de cette phase, les deux premières équipes sont qualifiées en winner bracket et les deux dernières vont en loser bracket. Les matchs de la phase finale
        servent à départager les places entre les équipes.
    </p>
    <img id="coupe" src="images/Version 256ko.png"/>
    <h2>Que puis-je voir ici ?</h2>
    <p class="p2">Ici tu peux à peu près tout voir ! La première section "Tournoi" sert à voir les informations du tournoi en cours. Tu peux y avoir la liste des équipes du mois ainsi que le bracket
        des différentes phases. Ensuite, tu peux aller voir les différents classements liés à l'OS Cup. Tout d'abord, le classement All Stars est le classement de tous les participants grâce aux
        points qu'ils gagnent à chaque tournoi. Le premier en gagne 20 et le dernier 0. Ainsi, les 12 premiers du classement seront qualifiés pour le tournoi All Stars de décembre.
    </p>
    <p class="p2">Le deuxième classement est le panthéon. Celui-ci classe les participants en fonction du nombre de médailles d'or, d'argent, de bronze, de loser et de participations. Ce classement
        n'a qu'un but honorifique et ne sert à rien d'autre. Uniquement pour le plaisir d'y retrouver son nom et de se rappeler de bons souvenirs :)
    </p>
    <p class="p2">Le troisième classement est le classement des pronostics. Chaque tournoi depuis août a lieu un sondage avant le tournoi afin de pronostiquer les résultats. Les points
        sont ensuite calculés en fonction des résultats finaux et un classement est fait ! Tout le monde peut participer à ce sondage, même ceux qui ne participent pas.
    </p>

    <p class="p2">Le dernier classement est en fait un classement de chaque joueur concernant les buts, les saves, les passes, les BO gagnés, matchs gagnés etc... A mettre en place</p>
