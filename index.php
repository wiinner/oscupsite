<!DOCTYPE html>
<?php
    require_once './utils/connect.php';
    require_once './class/userManagement.php';
    require_once './class/team.php';
    
    $bddconnect = new bddconnect;
    global $mysqli;
    $mysqli = $bddconnect->getConnection();

    $userManagement = new userManagement;

    $auth=explode(':',$_COOKIE['ZoneAdminOSCup']);
    if(!empty($auth[0]) || !empty($auth[1]) || !empty($auth[2]) || !empty($auth[3]) || !empty($auth[4])) {
        $Permission=$userManagement->VerifSession($auth[0],$auth[1], "player");
    
        if(!$Permission || $Permission != $auth[2]) {
            setcookie('ZoneAdminOSCup');
            header('Location: index.php');
            exit(); 
        }
    } elseif(isset($_POST['posted']) && $_POST["posted"]="1") {
        $userManagement->connectUser();
    }
?>
<html lang="fr">
    <head>
        <title>OS Cup</title>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="css/reset.css"/>
        <link rel="stylesheet" href="css/styles.css"/>
        <link rel="icon" type="image/png" href="images/logo.png"/>
    </head>


    <body>
        <div id="header">
            <a href="index.php" id="alogo"><img src="images/logo.png" alt="logo" id="logo"/></a>
            <nav>
                <ul>
                    <li class="deroulant"><a href="">Tournoi </a>
                        <ul class="sous">
                            <li><a href="?p=tournamentteam">Équipes</a></li>
                            <li><a href="?p=tournamentpool">Phases de poule</a></li>
                            <li><a href="">Phases finales</a></li>
                        </ul>
                    </li>
                    <li class="deroulant"><a href="">Classements </a>
                        <ul class="sous">
                            <li><a href="">Allstars Saison 1</a></li>
                            <li><a href="">Panthéon</a></li>
                            <li><a href="">Pronostics</a></li>
                            <li><a href="">Statistiques</a></li>
                        </ul>
                    </li>
                    <li class="deroulant"><a href="">Archives </a>
                        <ul class="sous">
                            <li><a href="">Saison 1</a></li>
                            <li><a href="">Saison 2</a></li>
                        </ul>
                    </li>
                    <li><a href="">Communauté</a></li>
                </ul>
            </nav>
            <?php if(empty($auth[0])): ?>
            <a href="?p=login" id="connexion">Connexion</a>
            <?php endif; ?>
            <?php if(!empty($auth[0])): ?>
            <a href="?p=login" id="connexion">Mon compte</a>
            <?php endif; ?>
        </div>

        <div id="main">
            <?php
            $p = "";
            if(isset($_GET["p"]))
            { 
                $p = $_GET['p'];
            }

            switch($p)
            {
                case "login":
                    include_once 'pages/login.php';
                    break;
                case 'tournament':
                    include_once 'pages/tournament.php';
                    break;
                case 'tournamentteam':
                    include_once 'pages/tournamentTeam.php';
                    break;
                case 'tournamentpool':
                    include_once 'pages/tournamentPool.php';
                    break;
                case 'phpinfo':
                    include_once 'pages/phpinfo.php';
                    break;
                case 'addtournament':
                    include_once 'pages/addtournament.php';
                    break;
                case 'addpool':
                    include_once 'pages/addpool.php';
                    break;
                case 'addteam':
                    include_once 'pages/addteam.php';
                    break;
                case 'addplayer':
                    include_once 'pages/addplayer.php';
                    break;
                case 'tournamentlist':
                    include_once 'pages/tournamentlist.php';
                    break;
                case 'managetournament':
                    include_once 'pages/managetournament.php';
                    break;
                default:
                    include_once 'main.php';
                    break;
            }
            ?>
        </div>
        <?php include_once 'footer.php' ?>
        
    </body>

</html>