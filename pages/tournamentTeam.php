<?php 
    include './class/tournament.php';

    $tournament = new Tournament;
    $tournament->getLastTournament();
    $tournamentArray = $tournament->getAllInfoTournament();
?>

<div id="tournamentTeam">
    <?php foreach($tournamentArray as $pool) { ?>
        <div class="PoolDisplay">
            <h1><?php echo $pool["name"]; ?></h1>
            <?php if(isset($pool["teams"])): ?>
                
                <?php foreach($pool["teams"] as $team) { ?>
                    <div class="TeamDisplay">
                    <h2><?php echo $team["name"]; ?></h2>
                    <?php foreach($team["Players"] as $player) { ?>
                        <h3><?php echo $player["name"]; ?>
                    <?php } ?>
                    </div>
                <?php } ?>
            <?php endif; ?>
        </div>
    <?php } ?>
    
</div>