<?php 
    include './class/tournament.php';

    $tournament = new Tournament;
    $tournament->getLastTournament();
    $tournamentArray = $tournament->getAllInfoTournament();
?>

<div id="tournamentTeam">
    <?php foreach($tournamentArray as $pool) { ?>
        <div class="PoolDisplay">
            <h1><?php echo $pool["name"]; ?></h1>
            <?php if(isset($pool["matchs"])): ?>
                
                <?php foreach($pool["matchs"] as $match) { ?>
                    <div class="TeamDisplay">
                    <h2><?php echo $match["team1_name"]; ?> <span class='vs'>vs</span> <?php echo $match["team2_name"]; ?></h2>
                        <div class="ResultDisplay">
                            <?php if(isset($match['result'])): ?>
                            <?php foreach($match['result'] as $result) { ?>
                            <h3><?php echo $result['score_team1']; ?> <span class='vs'>-</span> <?php echo $result['score_team2']; ?></h3>
                            <?php } ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php } ?>
            <?php endif; ?>
        </div>
    <?php } ?>
    
</div>