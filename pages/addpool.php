<?php include './class/tournament.php'; ?>
<?php include './class/form.php'; ?>
<?php
    $player = new player;
    $errAccess = $player->requestToAccess("addPool") ? "Accès interdit à la page<br/>" : "";
    
    $form = new form;
    $tournamentid = $form->getPostOrGet('tournamentid');

    if($form->getPostOrGet("posted")=="1") {
        $pool = new Pool;
        if(isset($_POST["name"])) { $pool->setName($_POST["name"]); }
        $pool->tournament_id = $tournamentid;
        $pool->type = 1; //Type pool (pas bracket)

        $err = "Problème lors de la création de la poule<br/>";
        $id = 0;

        $id = $pool->save($err);
        if($id > 0) {
            $msg = "Poule créée correctement";
        }
    }

    if(isset($tournamentid))
    {
        $tournament = new Tournament;
        $tournament->getTournament($tournamentid);
        $tournamentArray = $tournament->getAllInfoTournament();
    }
    else 
    {
        $errAccess .= "Accès interdit à la page";
    }
    
?>

<?php if($auth[0]): ?>
<div id="title">
    <?php if(empty($errAccess)) {
        $title = "Ajouter une poule au tournoi ".$tournament->getName();
    } else {
        $title = "Acces interdit";
    }
    ?>
    <h1><?php echo $title ?></h1>
    <?php if (isset($_POST['posted']) && $_POST["posted"]=="1" && isset($msg) && (!empty($msg))): ?>
        <span class="confirm">
            <?php echo $msg; ?>
        </span>
    <?php endif; ?>
    <?php if(!empty($err)): ?>
        <span class='error'>
            <?php echo $err; ?>
        </span>
    <?php endif; ?> 
    <?php if (empty($errAccess)): ?>
    <table id='listElement'>
        <thead>
            <tr>
                <th>Poules existantes</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($tournamentArray as $pool) { ?>
            <tr>
                <td><?php echo $pool["name"]; ?></td>
            </tr>
            <?php 
            }
            ?>
        </tbody>
    </table>
    <form name="add" method="post" action="?p=addpool&tournamentid=<?php echo $tournamentid; ?>">
        <input type="hidden" name="posted" value="1" maxlength="20" />
        <input type="hidden" name="tournamentid" value="<?php echo $tournamentid; ?>"/>
        <dl>
            <dt><label for="name" id='name'>Nom de la poule</dt>
            <dd><input type='text' name="name" id='name' size='30' value='<?php if(isset($_POST['name'])) { echo $_POST['name']; } ?>' required></dd>
            <dt>Nom du tournoi</dt>
            <dd><?php echo $tournament->getName(); ?></dd>
        </dl>
        <input type='submit' value='Ok'/>
    </form>
    <?php endif; ?> 
</div>
<?php endif; ?>