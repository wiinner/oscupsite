<?php include './class/tournament.php'; ?>
<?php
    if(isset($_POST['tournamentid'])) 
    { 
        $tournament_id = $_POST['tournamentid']; 
    } else { 
        if(isset($_GET['tournamentid'])) 
        { 
            $tournament_id = $_GET['tournamentid']; 
        }
    }
    $tournament = new Tournament;
    $tournament->getTournament($tournament_id);
    $tournamentArray = $tournament->getAllInfoTournament();
    //print_r($tournamentArray);
?>
<div id="tournamentTeam">
    <h1><?php echo $tournament->name; ?> - <a href='?p=addpool&tournamentid=<?php echo $tournament_id; ?>'>Ajouter une poule</a></h1>
    <?php foreach($tournamentArray as $pool) { ?>
        <div class="PoolDisplay">
            <h1><?php echo $pool["name"]; ?> - <a href="?p=addteam&poolid=<?php echo $pool["id"]; ?>">Ajouter une équipe</a></h1>
            <?php if(isset($pool["teams"])): ?>
                
                <?php foreach($pool["teams"] as $team) { ?>
                    <div class="TeamDisplay">
                    <h2><?php echo $team["name"]; ?> - <a href="?p=addplayer&teamid=<?php echo $team["id"]; ?>">Ajouter joueur</a></h2>
                    <?php foreach($team["Players"] as $player) { ?>
                        <h3><?php echo $player["name"]; ?>
                    <?php } ?>
                    </div>
                <?php } ?>
            <?php endif; ?>
        </div>
    <?php } ?>
    
</div>

<div id="title">
    <h1><?php echo $tournament->getName(); ?></h1>
    <ul>
        <li>Lancer la phase de poule</li>
        <li>Lancer la phase finale</li>
        <li>Inscrire un résultat de match</li>
    </ul>
</div>