<?php include './class/tournament.php'; ?>
<?php include './class/form.php'; ?>
<?php
    $player = new player;
    $errAccess = $player->requestToAccess("addTeam") ? "Accès interdit à la page<br/>" : "";
    
    $form = new form;
    $poolid = $form->getPostOrGet('poolid');

    if($form->getPostOrGet("posted")=="1") {
        $team = new Team;
        if(isset($_POST["name"])) { $team->setName($_POST["name"]); }
        $team->pool_id = $poolid;

        $err = "Problème lors de la création de l'équipe<br/>";
        $id = 0;

        $id = $team->save($err, $poolid);
        if($id > 0) {
            $msg = "Equipe créée correctement";
        }
    }

    if(isset($poolid))
    {
        $pool = new Pool;
        $pool->getPool($poolid);
        $team = new Team;
        $teamArray = $team->getAllTeamFromPool($poolid);
    }
    else 
    {
        $errAccess .= "Accès interdit à la page";
    }
    
?>

<?php if($auth[0]): ?>
<div id="title">
    <?php if(empty($errAccess)) {
        $title = "Ajouter une équipe à la poule ".$pool->getName();
    } else {
        $title = "Acces interdit";
    }
    ?>
    <h1><?php echo $title ?></h1>
    <?php if (isset($_POST['posted']) && $_POST["posted"]=="1" && isset($msg) && (!empty($msg))): ?>
        <span class="confirm">
            <?php echo $msg; ?>
        </span>
    <?php endif; ?>
    <?php if(!empty($err)): ?>
        <span class='error'>
            <?php echo $err; ?>
        </span>
    <?php endif; ?> 
    <?php if (empty($errAccess)): ?>
    <table id='listElement'>
        <thead>
            <tr>
                <th>Team existantes</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($teamArray as $team) { ?>
            <tr>
                <td><?php echo $team["name"]; ?></td>
            </tr>
            <?php 
            }
            ?>
        </tbody>
    </table>
    <form name="add" method="post" action="?p=addteam&poolid=<?php echo $poolid; ?>">
        <input type="hidden" name="posted" value="1" maxlength="20" />
        <input type="hidden" name="poolid" value="<?php echo $poolid; ?>"/>
        <dl>
            <dt><label for="name" id='name'>Nom de la Team</dt>
            <dd><input type='text' name="name" id='name' size='30' value='<?php if(isset($_POST['name'])) { echo $_POST['name']; } ?>' required></dd>
            <dt>Nom de la poule</dt>
            <dd><?php echo $pool->getName(); ?></dd>
        </dl>
        <input type='submit' value='Ok'/>
    </form>
    <?php endif; ?> 
</div>
<?php endif; ?>