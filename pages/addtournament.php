<?php include './class/tournament.php'; ?>
<?php
    $tournament = new Tournament;
    if(isset($_POST['posted']) && $_POST["posted"]="1") {
        if(isset($_POST["name"])) { $tournament->setName($_POST["name"]); }
        if(isset($_POST["date"])) { 
            $tournament->setDate($_POST["date"]);
            $date = DateTime::createFromFormat("Y-m-d", $_POST["date"]);
            $day = (int)$date->format("d");
            $month = (int)$date->format("m");
            $year = (int)$date->format("Y");
            $tournament->setDay($day);
            $tournament->setMonth($month);
            $tournament->setYear($year);
        }
        if(isset($_POST["subscription_date_start"])) { $tournament->setSubscription_date_start($_POST["subscription_date_start"]); }
        if(isset($_POST["subscription_date_end"])) { $tournament->setSubscription_date_end($_POST["subscription_date_end"]); }
        $err = "Problème lors de la création du tournoi<br/>";
        if($tournament->save($err)) {
            $msg = "Tournoi créé correctement";
        };
    }
?>
<?php if($auth[0]): ?>
<div id="title">
    <h1>Ajouter un tournoi</h1>
    <?php if (isset($_POST['posted']) && $_POST["posted"]="1" && isset($msg) && (!empty($msg))): ?>
        <span class="confirm">
            <?php echo $msg; ?>
        </span>
    <?php endif; ?>
    <?php if(!empty($err)): ?>
        <span class='error'>
            <?php echo $err; ?>
        </span>
    <?php endif; ?> 
    <?php if (empty($msg)): ?>
    <form name="add" method="post" action="?p=addtournament">
        <input type="hidden" name="posted" value="1" maxlength="20" />
        <dl>
            <dt><label for="name" id='name'>Nom du tournoi</dt>
            <dd><input type='text' name="name" id='name' size='30' value='<?php if(isset($_POST['name'])) { echo $_POST['name']; } ?>' required></dd>
            <dt><label for='date'>Date du tournoi</dt>
            <dd><input type='date' name="date" id='date' size='8' value='<?php if(isset($_POST['date'])) { echo $_POST['date']; } ?>' required></dd>
            <dt><label for='subscription_date_start'>Date de début des inscriptions</dt>
            <dd><input type='date' name="subscription_date_start" id='subscription_date_start' size='10' value='<?php if(isset($_POST['subscription_date_start'])) { echo $_POST['subscription_date_start']; } ?>' required></dd>
            <dt><label for='subscription_date_end'>Date du tirage</dt>
            <dd><input type='date' name="subscription_date_end" id='subscription_date_end' size='10' value='<?php if(isset($_POST['subscription_date_end'])) { echo $_POST['subscription_date_end']; } ?>' required></dd>
        </dl>
        <input type='submit' value='Ok'/>
    </form>
    <?php endif; ?> 
</div>
<?php endif; ?>