<?php include './class/tournament.php'; ?>
<?php
    $tournament = new Tournament;
    $arrayTournament = $tournament->listAllTournament();
?>

<div id="title">
    <h1>Listes des tournois</h1>
    <table id='listElement'>
        <thead>
            <tr>
                <th>Nom du tournoi</th>
                <th>Date</th>
                <th>Date de début d'inscription</th>
                <th>Date du tirage</th>
                <th>Type de tournoi</th>
                <?php if(!empty($auth[0] && $auth[2]==18)): ?>
                    <th>Actions</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($arrayTournament as $tournamentData) { ?>
            <tr>
                <td><?php echo $tournamentData->getName(); ?></td>
                <td><?php echo $tournamentData->getDate(); ?></td>
                <td><?php echo $tournamentData->getSubscription_date_start(); ?></td>
                <td><?php echo $tournamentData->getSubscription_date_end(); ?></td>
                <td><?php echo $tournamentData->getTournamentTypeName(); ?></td>
                <?php if(!empty($auth[0] && $auth[2]==18)): ?>
                    <td><a href="?p=edittournament&tournamentid=<?php echo $tournamentData->getId(); ?>">Editer</a>&nbsp;<a href="?p=managetournament&tournamentid=<?php echo $tournamentData->getId(); ?>">Gérer</a></td>
                <?php endif; ?>
            </tr>
        </tbody>
        <?php } ?>
    </table>
    
</div>