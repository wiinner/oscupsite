<?php include './class/tournament.php'; ?>
<?php include './class/form.php'; ?>
<?php
    $player = new player;
    $errAccess = $player->requestToAccess("addPlayer") ? "Accès interdit à la page<br/>" : "";
    
    $form = new form;
    $teamid = $form->getPostOrGet('teamid');

    if($form->getPostOrGet("posted")=="1") {
        $teamplayer = new TeamPlayer;
        if(isset($_POST["player_id"])) { $teamplayer->player_id = $_POST["player_id"]; }
        $teamplayer->team_id = $teamid;

        $err = "Problème lors de l'ajout du joueur à l'équipe<br/>";
        $id = 0;

        $id = $teamplayer->save($err);
        if($id > 0) {
            $msg = "Joueur ajouté correctement";
        }
    }

    if(isset($teamid))
    {
        $teamPlayer = new TeamPlayer;
        $teamPlayerArray = $teamPlayer->getAllPlayerFromTeam($teamid);
        $team = new Team;
        $team->getTeam($teamid);
        $form = new Form;
    }
    else 
    {
        $errAccess .= "Accès interdit à la page";
    }
    
?>

<?php if($auth[0]): ?>
<div id="title">
    <?php if(empty($errAccess)) {
        $title = "Ajouter un joueur à l'équipe ".$team->name;
    } else {
        $title = "Acces interdit";
    }
    ?>
    <h1><?php echo $title ?></h1>
    <?php if (isset($_POST['posted']) && $_POST["posted"]=="1" && isset($msg) && (!empty($msg))): ?>
        <span class="confirm">
            <?php echo $msg; ?>
        </span>
    <?php endif; ?>
    <?php if(!empty($err)): ?>
        <span class='error'>
            <?php echo $err; ?>
        </span>
    <?php endif; ?> 
    <?php if (empty($errAccess)): ?>
    <table id='listElement'>
        <thead>
            <tr>
                <th>Joueurs existants</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($teamPlayerArray as $teamplayer) { ?>
            <tr>
                <td><?php echo $teamplayer["player_name"]; ?></td>
            </tr>
            <?php 
            }
            ?>
        </tbody>
    </table>
    <form name="add" method="post" action="?p=addplayer&teamid=<?php echo $teamid; ?>">
        <input type="hidden" name="posted" value="1" maxlength="20" />
        <input type="hidden" name="teamid" value="<?php echo $teamid; ?>"/>
        <dl>
            <dt><label for="name" id='name'>Sélection du joueur</dt>
            <dd><?php echo $form->selectFromTable('player', 'player_id', 'id', 'name'); ?></dd>
            <dt>Nom de la team</dt>
            <dd><?php echo $team->name; ?></dd>
        </dl>
        <input type='submit' value='Ok'/>
    </form>
    <?php endif; ?> 
</div>
<?php endif; ?>