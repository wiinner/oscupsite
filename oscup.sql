-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 16 nov. 2020 à 10:58
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `oscup`
--

-- --------------------------------------------------------

--
-- Structure de la table `matchresult`
--

DROP TABLE IF EXISTS `matchresult`;
CREATE TABLE IF NOT EXISTS `matchresult` (
  `match_id` int(11) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `bo_number` int(4) NOT NULL,
  `team1_id` int(11) NOT NULL,
  `team2_id` int(11) NOT NULL,
  `score_team1_id` int(11) DEFAULT NULL,
  `score_team2_id` int(11) DEFAULT NULL,
  `depth` int(4) NOT NULL,
  PRIMARY KEY (`bo_number`,`team1_id`,`team2_id`,`pool_id`) USING BTREE,
  KEY `MatchResult_Match` (`match_id`),
  KEY `fk_pool_id` (`pool_id`),
  KEY `team1_id` (`team1_id`),
  KEY `team2_id` (`team2_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `matchresult`
--

INSERT INTO `matchresult` (`match_id`, `pool_id`, `bo_number`, `team1_id`, `team2_id`, `score_team1_id`, `score_team2_id`, `depth`) VALUES
(1, 1, 1, 1, 2, 1, 3, 1),
(1, 1, 2, 1, 2, 3, 2, 1),
(1, 1, 3, 1, 2, 3, 1, 1),
(1, 1, 4, 1, 2, 3, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `player`
--

DROP TABLE IF EXISTS `player`;
CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `discord` varchar(64) NOT NULL,
  `steamid` varchar(128) NOT NULL,
  `epicid` varchar(128) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Player_ak_1` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `player`
--

INSERT INTO `player` (`id`, `name`, `email`, `password`, `discord`, `steamid`, `epicid`, `status`) VALUES
(1, 'Oasix', '', '', '', '', '', 1),
(2, 'Onizukratos', '', '', '', '', '', 1),
(3, 'iceman', '', '5f4dcc3b5aa765d61d8327deb882cf99', '', '', '', 18);

-- --------------------------------------------------------

--
-- Structure de la table `pool`
--

DROP TABLE IF EXISTS `pool`;
CREATE TABLE IF NOT EXISTS `pool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`,`name`,`tournament_id`) USING BTREE,
  UNIQUE KEY `tournament_id` (`tournament_id`,`name`,`type`),
  KEY `fk_tournament_id` (`tournament_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pool`
--

INSERT INTO `pool` (`id`, `tournament_id`, `name`, `type`) VALUES
(1, 1, 'Poule A', 1),
(2, 1, 'Poule B', 1);

-- --------------------------------------------------------

--
-- Structure de la table `poolranking`
--

DROP TABLE IF EXISTS `poolranking`;
CREATE TABLE IF NOT EXISTS `poolranking` (
  `id` int(11) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `ranking` int(11) NOT NULL,
  PRIMARY KEY (`id`,`pool_id`,`team_id`),
  KEY `PoolRanking_Pool` (`pool_id`),
  KEY `team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_poolid` (`pool_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `team`
--

INSERT INTO `team` (`id`, `pool_id`, `name`) VALUES
(1, 1, 'Team Oasix'),
(2, 1, 'Team Rheoklash'),
(3, 1, 'Team Grenzo'),
(4, 1, 'Team Wise'),
(5, 2, 'Team Zuyute'),
(7, 2, 'Team iceman');

-- --------------------------------------------------------

--
-- Structure de la table `teamplayer`
--

DROP TABLE IF EXISTS `teamplayer`;
CREATE TABLE IF NOT EXISTS `teamplayer` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`player_id`,`team_id`),
  KEY `TeamPlayer_Team` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `teamplayer`
--

INSERT INTO `teamplayer` (`player_id`, `team_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tournament`
--

DROP TABLE IF EXISTS `tournament`;
CREATE TABLE IF NOT EXISTS `tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `date` date NOT NULL,
  `subscription_date_start` datetime NOT NULL,
  `subscription_date_end` datetime NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `tournament_type_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tournament`
--

INSERT INTO `tournament` (`id`, `name`, `date`, `subscription_date_start`, `subscription_date_end`, `month`, `year`, `day`, `tournament_type_id`) VALUES
(1, 'Test tournament', '2020-10-12', '2020-10-12 00:00:00', '2020-10-13 00:00:00', 10, 2020, 12, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tournamentranking`
--

DROP TABLE IF EXISTS `tournamentranking`;
CREATE TABLE IF NOT EXISTS `tournamentranking` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `ranking` int(11) NOT NULL,
  PRIMARY KEY (`id`,`tournament_id`,`team_id`),
  KEY `TournamentRanking_Tournament` (`tournament_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tournamenttype`
--

DROP TABLE IF EXISTS `tournamenttype`;
CREATE TABLE IF NOT EXISTS `tournamenttype` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tournamenttype`
--

INSERT INTO `tournamenttype` (`id`, `name`) VALUES
(1, 'Normal');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `matchresult`
--
ALTER TABLE `matchresult`
  ADD CONSTRAINT `fk_pool_id` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matchresult_ibfk_1` FOREIGN KEY (`team1_id`) REFERENCES `team` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matchresult_ibfk_2` FOREIGN KEY (`team2_id`) REFERENCES `team` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `pool`
--
ALTER TABLE `pool`
  ADD CONSTRAINT `pool_ibfk_1` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `poolranking`
--
ALTER TABLE `poolranking`
  ADD CONSTRAINT `poolranking_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `poolranking_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `team_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `teamplayer`
--
ALTER TABLE `teamplayer`
  ADD CONSTRAINT `teamplayer_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `teamplayer_ibfk_2` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `tournamentranking`
--
ALTER TABLE `tournamentranking`
  ADD CONSTRAINT `tournamentranking_ibfk_1` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
